# Docker Compose

Docker Compose是Docker底下的其中一個工具，他可以一次管理複數個docker container的建立、啟動、停止、刪除等操作。如果你的系統長的像下圖那樣的複雜，而每個模組都是一個容器的話，Docker Compose就能夠很方便的管理這一大堆的容器了。

![](https://www.oreilly.com/library/view/microservices-with-docker/9780134218229/graphics/03fig02.jpg)

我們雖然沒有需求一次管理這麼多容器，但Docker Compose還是很需要。當你每次要建立容器時都要打落落長的指令加入gpu、加入掛載點、加入port forward等等等參數，這是非常繁瑣的。Docker Compose可以讀取yaml檔裡的設定，使用者只要把前面繁瑣的參數定義在yaml檔中，啟動容器就只需要一行簡單的`docker compose up`就完成了。


## 安裝Docker Compose

Ubuntu使用者

```bash
sudo apt-get install docker-compose-plugin
```

Archlinux或Manjaro使用者

```bash
pacman -S docker-compose
# 或是
pamac install docker-compose
```

測試Docker compose有沒有安裝成功

```bash
docker compose version
```


## 幫專案寫一個Compose file

Docker Compose要看的檔案是一個yaml檔，如果沒有聽過yaml檔的話，這邊有[傳送門](https://zh.wikipedia.org/zh-tw/YAML)可以去wiki看一下yaml檔的介紹。yaml檔是一種文字檔的格式，可讀性很高，在很多程式語言都有函式庫可以讀寫yaml檔。

在Dockerfile的章節教學的時候，我們建立了一個映像檔，叫做simple_image，而我們要建立容器時，輸入了下面的`docker`指令：

```bash
cd projects/useless_project
docker run -it -v ${PWD}:/workspace simple_image
```

將上面的指令寫成Compose file就會像下方這樣，請建立一個檔案名為`compose.yml`，並寫入以下內容。

```yaml
services:
  worker:
    image: simple_image

    stdin_open: true
    tty: true
    volumes:
      - .:/workspace
```

1. `compose.yml`是docker compose指令預設在資料夾中尋找的設定檔。
2. `services`中會宣告要啟動的服務(容器)，這邊我們自訂一個名叫`worker`的容器，接著宣告此容器的內容，這邊可以寫不只一個服務。
3. `image: simple_image`代表我們要以這個映像檔來建立容器。
4. `volumes`相當於docker run後面的`-v`，volumes接受的是list，可以包含很多對的掛載路徑，範例上把當前目錄掛載至容器中的workspace路徑。
5. `stdin_open: true` 和 `tty: true` 分別相當於docker run後面的`-i`和`-t`。此容器建立後，如果attach的話就能在bash輸入指令。這兩個是需要的，可以讓bash卡在等待輸入的狀態。否則要修改entrypoint想辦法不要讓容器馬上停止。
6. 如果你學過docker compose的，會發現沒有寫`version: "3"`，因為這已被列為過時的語法了。
7. 如果你是學過的，現在docker compose最優先尋找檔名已經改成`compose.yaml`或`compose.yml`。`docker-compose.yml`為了向下相容，也還可以使用。

接著若要啟動容器，只要在這個目錄下用一行簡單的`up`指令，會將`compose.yml`裡定義的容器全部**建立+啟動**，`-d`參數代表將容器在背景執行。

```bash
docker compose up -d
```

相反的，若要關閉這些容器，也是在這個目錄下輸入`down`指令，會將`compose.yml`裡定義的容器全部**停止+刪除**。因為會刪除容器，所以容器裡的資料也會刪除。

```bash
docker compose down
```

若要查看這些容器的狀態，可以使用`ps`指令。

```bash
docker compose ps
```

若要進入容器，就用`exec`指令進入。

```bash
docker compose exec <服務名稱> <要執行的指令>...

#例如
docker compose exec worker bash
```

![compose_img01.png](assets/compose_img01.png)

原本要輸入`docker run`之後加上各種參數才能啟動一個容器，下次要在別台電腦上再啟動容器，也要再輸入一次這些參數。寫成Compose file之後，你的大腦就可以不用記下這些參數。用`docker compose`的指令多半不用再打參數，因為原本的參數你都已經寫在Compose file裡了。

你可以把Compose file放在你在寫的專案裡，當你需要測試這專案時候，就可以快速的啟動容器，在容器中跑這些測試程式。


## 需要GPU資源的Compose file

如果你正在做的專案是需要GPU來執行的，要求GPU資源在Compose file的寫法較為複雜。這次以pytorch映像檔來作為範例，我們用`docker run`的時候使用了以下指令。

```bash
cd projects/basic_training

docker run -it --gpus all -v ${PWD}:/workspace --shm-size=1gb pytorch/pytorch:1.11.0-cuda11.3-cudnn8-runtime bash
```

寫成Compose file可寫成以下這樣。一樣在此資料夾下建立`compose.yml`檔案。

```yaml
services:
  pytorch:
    image: pytorch/pytorch:1.11.0-cuda11.3-cudnn8-runtime

    entrypoint: ["sleep", "inf"]
    #stdin_open: true
    #tty: true
    shm_size: 1gb
    deploy:
      resources:
        reservations:
          devices:
            - driver: nvidia
              count: all
              capabilities: [gpu]
    volumes:
      - .:/workspace
```

1. 這邊把服務名子訂成`pytorch`，可以隨自己需求改名子。
2. 覆寫了原本映像檔的`entrypoint`，程式的進入點變成`sleep inf`，也可以讓映像檔處於執行中狀態。
3. 增加了`deploy`和底下的內容，這些即是宣告容器建立時需要GPU資源。
4. 增加了`shm_size: 1gb`是讓docker增加memory disk的空間，讓pytorch可以使用。

跟上面一樣的`up`指令建立容器並啟動，接著使用`exec`指令進入到容器中，再執行訓練程式。

```bash
docker compose up -d 
docker compose exec pytorch bash

# 進到容器後
python train_cnn.py
```

![compose_img02.png](assets/compose_img02.png)

若你使用的夠熟悉以後，可以從`exec`指令後面執行你的進入點，像是下方這個樣子。這樣訓練程式在容器中跑完之後就會退出容器環境。

```bash
docker compose up -d
docker compose exec pytorch python train_cnn.py
``` 
![compose_img03.png](assets/compose_img03.png)

如果要讓你的訓練pipeline要更加順暢，除了把參數都預先設定好之外，可以直接把`compose.yml`裡的`entrypoint`改成你的程式進入點，這樣`docker compose up -d`之後你的訓練程式就開始執行了，你可以用`docker compose logs`去監看他的執行狀態。

```diff
--- a/projects/basic_training/compose.yml
+++ b/projects/basic_training/compose.yml
@@ -2,7 +2,7 @@ services:
   pytorch:
     image: pytorch/pytorch:1.11.0-cuda11.3-cudnn8-runtime
 
-    entrypoint: ["sleep", "inf"]
+    entrypoint: ["python", "train_cnn.py"]
     #stdin_open: true
     #tty: true
     shm_size: 1gb
```


## 用Compose file來製作映像檔

前面的兩個範例，都是舉例把Compose file放在開發中的專案。這邊要把Compose file跟映像檔製作的專案放在一起，原本你要打`docker build`後面還要接上映像檔名稱的，用Compose file記錄下來的話，這樣你連映像檔名稱都不用記了。

`projects`的資料夾下，有`simple_image`, `alexeyab_darknet`, `pytorch_yolov5`這三個專案都是用來建立映像檔的，裡面也都寫了`compose.yml`。只要進資料夾用`docker compose build`就能夠製作出映像檔了。

```bash
cd projects/simple_image
cat compose.yml
```

```yaml
services:
  image-build:
    image: simple_image
    build: .
```

主要是`build: .`的敘述，指定要在當前資料夾下做映像檔。製作好的映像檔名稱就會命名成`image: simple_image`。

```bash
docker compose build

# 如果你的映像檔名稱是可上傳的格式的話，可以用以下指令直接上傳。
docker compose push
```


## 其他常用的指令

```bash
# 可以看看help裡面寫有哪些功能
docker compose --help

# 建立+啟動容器
docker compose up

# 停止+刪除容器
docker compose down

# 啟動容器
docker compose start

# 停止容器
docker compose stop

# 重新啟動容器
docker compose restart

# 製作映像檔
docker compose build

# 下載所有的映像檔
docker compose pull

# 上傳所有映像檔
docker compose push

# 查看容器的狀態
docker compose ps

# 看容器的log
docker compose logs

# 在容器中執行程式
docker compose exec <service> <command>...
```


## 其他參考資料

- YAML檔介紹：https://zh.wikipedia.org/zh-tw/YAML
- Overview of Docker Compose: https://docs.docker.com/compose/
- GPU support in Compose: https://docs.docker.com/compose/gpu-support/
- Compose specification: https://docs.docker.com/compose/compose-file/
- Overview of docker-compose CLI: https://docs.docker.com/compose/reference/
