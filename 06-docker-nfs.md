# Docker + Shared Storage

當你在做實驗，把訓練程式放到多台電腦裡面去跑，結果跑一跑發現有bug，程式要改一改。改完之後你要再重新把訓練程式發送到每一台電腦裡，再把訓練程式重開。最後終於訓練完成了，訓練完之後呢，你要把每一台電腦的訓練結果收回來處理。雖然用docker可以輕易的把環境搬到別的電腦，但是你的程式碼要再用另外的方式放進去才行。如果可以把某個共同的空間掛載到容器裡就好了。

這時就要善用docker volume的功能，一般建立volume的時候只會把主機上的目錄作為volume，其實跟bind mount沒兩樣，只是掛載的來源資料夾是Docker選的或自己選的差別而已。不過docker volume其實支援從NFS或是其他協定掛載，安裝Volume plugin還可以擴充更多協定。Docker預設的volume driver可以支援用NFS, Samba掛載，至少官網只有寫這兩個協定，不確定有沒有更多。官網也寫到，如果要用sshfs的話，安裝個plugin就可以支援。

本章節的範例會示範用NFS，如果你對NFS不熟悉，建議要先去看看NFS是什麼東東。這邊我用一台電腦當server端分享空間，兩台電腦當client端來掛載同一個空間。因為我沒有這麼多電腦，所以是開3台虛擬機來模擬這樣的環境。實務上你會用自己的電腦當nfs server來分享空間，裡面放程式碼，訓練結果等資料。有GPU的訓練機則作為client端掛載資料進去跑程式。

![nfs_img01.png](assets/nfs_img01.drawio.png)

用NFS的時候要注意幾件事情：

1. 用NFS的時候建議在同一個網段下，至少client端要找的到server端才能建立連線。
2. NFS的協定沒有認證機制，沒有帳號密碼。你只能用限制IP的方式來防止外人掛載你的空間。尤其是有public IP的電腦要用來開nfs server的話，更要小心這件事情。
3. 當還有client端還掛載著的時候，server不可以關掉，否則client端會跟著crash，也可能整台電腦卡住無法動彈。
4. NFS開server的時候是需要kernel module支援的，需要主機端有kernel module，要用容器開server的話也需要privileged mode給更大的權限，容器才能正常啟動。通常ubuntu已經會內建nfs的module，即便你沒有安裝nfs的套件在主機上也沒關係。

## 建立3台虛擬機

這邊用Linux KVM + Terraform來建立虛擬機。你也可以用其他的方式開虛擬機，或是直接找電腦來用，電腦裡只要灌好docker和docker compose就可以。

```bash
cd projects/docker_shared_storage

# 下載ubuntu的虛擬機映像檔
./download_ubuntu.sh

# 讓terraform來建立虛擬機
terraform init
terraform apply
```

![nfs_img02.png](assets/nfs_img02.png)
![nfs_img03.png](assets/nfs_img03.png)

這樣會建立三台ubuntu虛擬機，這三台虛擬機裡會灌好docker，他們的hostname會設定成`nfsserver`, `client1`, `client2`，為方便起見，後面連線的時候會用`nfsserver.local`這樣的方式去連線，當然要用IP也是可以。他們的登入帳號密碼是`ubuntu`和`password`，也可以用金鑰連線。

```bash
./send_data.sh
```

上述的shell script會把nfs_server的專案傳給server主機，nfs_client的專案傳給client主機。如果你是用自己電腦的，就自己傳一下。

![nfs_img04.png](assets/nfs_img04.png)


## 設定nfs server

接下來要先建立NFS的server端。如果是要在主機上安裝NFS server的話，以ubuntu來說就是：

```bash
sudo apt install nfs-kernel-server
```

接著修改`/etc/exports`檔案，寫上你要分享的目錄資訊。接著用下方的指令來分享目錄：

```bash
exportfs -arv
```

如果要看目前分享的情況：

```bash
exportfs -v
```

這邊的範例還是以docker compose來建立nfs server，server的範例放在`nfs_server`資料夾中。

```bash
cd projects/docker_shared_storage/nfs_server

cat compose.yml
```

```yaml
services:
  nfs:
    image: itsthenetwork/nfs-server-alpine
    environment:
      - SHARED_DIRECTORY=/nfs
      - SYNC=true
      - PERMITTED="192.168.122.*"
    volumes:
      - ./data:/nfs
    privileged: true
    ports:
      - 2049:2049
```

這個映像檔來自這裡：https://hub.docker.com/r/itsthenetwork/nfs-server-alpine

這邊的compose file使用了網路大神做的nfs server映像檔。他會開一個nfs v4的server，那些環境變數的設定方式都是參考作者寫的說明，主要是要指定分享的目錄及對象。

1. 設定要分享的根目錄是`/nfs`
2. 設定NFS是同步的模式，當使用者寫入檔案時會立刻寫入硬碟中。
3. 限定IP的白名單是`192.168.122.*`，如果你使用的網段不相同，則要自己修改成自己的網段。
4. 用bind mount把主機的`./data`目錄掛載進容器中的`/nfs`，所以data目錄中的資料會被分享出去。
5. 設定這個容器是privileged mode，NFS才能正常啟動。
6. 設定2049 port的轉port，是nfs v4會使用到的port。

我們進入剛剛的虛擬機，並把nfs server打開。

```bash
ssh -i id_ed25519 ubuntu@nfsserver.local

cd nfs_server/
sudo docker compose up -d
sudo docker compose logs
```

![nfs_img05.png](assets/nfs_img05.png)

正常啟動應該會像這樣沒有錯誤訊息，這樣我們就有一個nfs server了。


## 建立client

client端的主機一樣只需要安裝docker就可以，主機上不需要安裝`nfs-common`套件。如果想要測試前面的server能不能使用的話，也可以安裝一下，試著掛載看看。正常掛載會沒有任何訊息，掛載成功就可以在資料夾裡放點東西測試。

```bash
mkdir -p nfs_shared/
sudo mount -t nfs -o vers=4 nfsserver.local:/ nfs_shared

mount | grep nfs_shared

# 取消掛載
sudo umount nfs_shared/
```

接下來我們要用docker來建立volume並掛載到容器中。

```bash
cd projects/docker_shared_storage/nfs_client

cat compose.yml
```

```yaml
services:
  py:
    image: python:alpine

    tty: true
    entrypoint: ['python', 'do_tasks.py']
    volumes:
      - shared-data:/shared_data
      - .:/workspace
    environment:
      - TASKS_PATH=/shared_data
    working_dir: /workspace

volumes:
  shared-data:
    driver: local
    driver_opts:
      type: "nfs"
      o: "addr=nfsserver.local,nfsvers=4,soft,rw"
      device: ":/"
```

這裡的重點在後半段`volumes`的部份：

1. 這邊建立一個volume叫做`shared-data`
2. 指定建立volume使用的driver是`local`這是預設的，docker內建的。
3. 掛載的選項寫在`driver_opts`裡
4. 指定掛載的類型是nfs
5. 指定掛載的來源是nfsserver.local，各位可以自行改成其他IP。
6. 指定掛載的nfs版本是v4版本，v4版本的nfs只需要tcp 2049 port。
7. 指定掛載來源的目錄是分享的根目錄，這邊就會對應到server端設定的`/nfs`因為這是分享出來的根目錄，如果要進一步指定子目錄也可以自行修改。
8. 容器上設定了volumes，把`shared-data`這個volume掛載到`/shared_data`，我們可以在這資料夾下找到分享的檔案

```bash
ssh -i id_ed25519 ubuntu@client1.local

cd nfs_client/
sudo docker compose up -d
```

如果NFS被成功掛載了，容器才會啟動。如果NFS掛載失敗如下圖，容器就不會啟動，你要檢查你的server端是不是有正確開啟、有沒有正確轉port、NFS版本是不是v4、白名單是不是擋到這個ip等等問題。

![nfs_img06.png](assets/nfs_img06.png)

```bash
sudo docker compose exec py sh

cd /shared_data
ls -al
```

![nfs_img07.png](assets/nfs_img07.png)

用exec進入到容器中，切換目錄到`/shared_data`下，應該至少會看到一個`.gitkeep`檔案，這是server端這資料夾裡原本放的，已經在client端可以看到。你也可以試試新增一些檔案，看看檔案會不會送到server端。

測試完之後，可以用down指令把容器刪除，不過預設volume不會被刪掉。若有要修改nfs server的掛載資訊，要記得把volume刪掉(加`-v`參數)，下次up時volume才會重建，新參數才會生效。

```bash
sudo docker compose down

sudo docker compose down -v
```


## 簡單用`lockf`的功能來排程工作

`lockf`顧名思義就是可以鎖定一個檔案，不過不是真的鎖住讓別人都無法讀寫。當有人用`lockf`鎖定這個檔案後，另一個人就無法用`lockf`來鎖定檔案，會發生失敗或是進入等待。`lockf`的功能，可以讓檔案一次只能有一人存取。善用這個特性，我們可以建立工作排程，並且可以確保一個工作只會被一個人拿去執行。

一般來說`lockf`大概只能在同一台電腦裡有效，不過NFS有處理好`lockf`的功能，使`lockf`的訊息可以在每個NFS掛載點之間傳遞，已經被A主機鎖定的檔案，若在B主機想再鎖定會失敗，鎖定可以跨主機。如果你是用sshfs或是smb來分享目錄的話，我就不知道有沒有支援`lockf`了。

把剛才的兩個client都啟動，容器的進入點已經改成資料夾內的python腳本。這個python腳本會從資料夾內領取工作，並且做sleep當作在做事情，做完以後會再接下一個工作來做。client會用`lockf`的方式確認工作是不是被領走，所以兩個client不會做到同一個工作。

```bash
ssh -i id_ed25519 ubuntu@client1.local
cd nfs_client
sudo docker compose up

ssh -i id_ed25519 ubuntu@client2.local
cd nfs_client
sudo docker compose up

ssh -i id_ed25519 ubuntu@nfsserver.local
cd nfs_server/
./generate_tasks.py
```

執行的結果可能如下圖

![nfs_img08.png](assets/nfs_img08.png)

可以看到client1做了task1, 3, 4，client2做了task2, 5。

這兩個client會接受像下方的資料夾結構，client會確認`status.txt`能不能用lockf鎖住，若成功鎖住，表示可以開始做這項工作。若無法鎖住，則跳過。當事情做完之後，會把`status.txt`改名成`status_complete.txt`當作這項工作已經做完，再把檔案解鎖。

```text
task0001
├── parameter.yml
└── status.txt      # rename to status_complete.txt after complete.
```

實做的方式可以看client裡面的python腳本，裡面有寫一些註解。

因為做實驗的時候總是要把工作灑出去到各台訓練機中，訓練完的結果又要再收集起來做整理。如果有NFS去分享資料，你就可以讓每台訓練機用某種方式領取屬於自己的工作，完成自己的部份，完成之後把結果統一放好。

至於訓練機要如何判別是不是自己的工作，大家可以視自己情況實做，最簡單的方式可能是分n個資料夾給n個訓練機，也可能是在參數中指定主機id，也可以像本章節的方式，用`lockf`的功能判別工作是否被領走，沒被領走就拿來做。


## 刪除虛擬機

收尾工作，把虛擬機關機刪掉。

```bash
terraform destroy
```
