# Docker基本指令介紹(Docker cli)

### 解釋一下`hello-world`

剛剛執行的`hello-world`範例，Docker做了幾件事情：

1. Docker client連線到Docker engine(Docker daemon)。
2. Docker daemon在本地端尋找`hello-world`這個映像檔(image)，但找不到，於是嘗試上Docker Hub去**拉**下來。
3. Docker daemon用那個image建立一個新的container，然後執行裡面的程式。
4. Docker daemon把stdout的內容送給Docker client，client端再顯示在螢幕上。


### 試試看Docker官方提供的ubuntu的映像檔

接下來試試看下載一個Ubuntu的映像檔，然後執行裡面的bash。可以在裡面嘗試輸入一些指令，探索一下映像檔裡面有什麼。

```bash
docker run -i -t ubuntu bash
```

1. 映像檔裡面很乾淨，幾乎只有基本的linux指令可以使用，額外的套件都沒有安裝。不過你有apt可以用，可以自由安裝套件。
2. `docker run`每次都會建立一個新的容器。
3. 在映像檔後面加的`bash`表示覆寫預設的容器進入點，指定要執行映像檔裡面的`bash`
4. 指令中的`-i -t`能夠讓使用者保留在容器裡面，可以對bash下指令；而不是像`hello-world`那個範例，在瞬間執行完之後就退出容器。

也試試別的官方映像檔吧

```bash
# Python的映像檔，一進去就會是python的直譯器環境中
docker run -it python

# Alpine是輕量型的的linux系統，體積相較於Ubuntu或Debian來的小很多。
docker run -it alpine
```


### 列出映像檔、容器

剛剛提到了 下載映像檔 還有 建立container 這兩件事情。

映像檔就像是一個模板或是安裝光碟，可以用映像檔來建立無數個容器，映像檔可以從網路上下載，也可以自己製作。而Docker Hub是個Docker image的repository，上面有官方維護的映像檔，每個人也都能上傳自己製作的映像檔。

用`docker images`查看現在本地端的映像檔。

```bash
docker images
```

![cli_images01.png](assets/cli_images01.png)

可以看到列出了五個欄位：

1. REPOSITORY: 映像檔的名子
2. TAG: 映像檔的Tag，或者是版本號等等。
3. IMAGE ID: 映像檔的ID，以映像檔內容算出的hash，每一個映像檔都會有一個獨特的ID，如果ID一樣代表是同個映像檔。
4. CREATED: 映像檔是哪時由作者做出來的。
5. SIZE: 映像檔的原始大小。


容器可以想像成被你建立的虛擬機，建立成容器之後才能被執行。容器關閉之後，資料會暫時留在容器裡面，刪除容器之後，執行過的內容就會被刪除。建立容器的時候，可以把主機的資料夾掛載進容器，這樣就能在主機跟容器之間交換資料。

用`docker ps`查看現在執行中的容器，本來只會列出執行中的容器，加上`-a`的參數可以把已停止的容器一起列出。

```bash
docker ps

docker ps -a
```

![cli_ps01.png](assets/cli_ps01.png)

列出容器的時候會列出以下欄位：

1. CONTAINER ID: 容器的ID，每個容器都有隨機ID。要操作指定容器的時候，可以用CONTAINER ID來指定。
2. IMAGE: 該容器是以哪個映像檔建立的。
3. COMMAND: 這個容器的程式進入點。
4. CREATED: 容器的建立時間。
5. STATUS: 這個容器的狀態，執行中或已停止等。
6. PORTS: 容器的port forward資訊
7. NAMES: 這個容器的名子，建立容器時可以指定，沒有指定則會隨機產生。要操作指定容器的時候，可以用NAMES來指定。


### 刪除容器、映像檔

刪除映像檔的時候，要把由這個映像檔建立的容器先全數停止和刪除，才能刪除映像檔。當你要指定一個映像檔的時候，可以用`REPOSITORY:TAG`的格式或`IMAGE ID`欄位來指定映像檔。指定多個名子可以一次刪除多個映像檔。

```bash
docker rmi <映像檔名稱 或者ID>

# 例如
docker rmi ubuntu:latest ubuntu:22.04 8a0322e31ad1
```

要刪除容器的時候，要先將容器停止，才能刪除容器。當你要指定容器的時候，可以用`CONTAINER ID`或是`NAMES`欄位來指定。當然也可以指定多組，就會一次刪除多個容器。

```bash
docker rm <容器名稱 或者ID>

# 例如
docker rm d3ba07bedad musing_einstein
```

![cli_rm_01.png](assets/cli_rm01.png)

![cli_rm_02.png](assets/cli_rm02.png)


### 建立容器時掛載主機內容

容器內部是獨立環境，但典型的使用方式一定會掛載使用者資料進到容器內部使用，容器建立時可以使用`-v`參數掛載資料進去。掛載有兩種方式，一種是bind mount，另一種是使用volume，比較如下：

|                              | Named Volumes               | Bind Mounts                     |
| ---------------------------- | --------------------------- | ------------------------------- |
| 存放位置                     | Docker的資料存放區          | 使用者自己控制                  |
| 掛載範例(用`-v`)             | `my-volume:/usr/local/data` | `/path/to/data:/usr/local/data` |
| 產生新volume時放入映像檔內容 | Yes                         | No                              |
| 支援volume driver            | Yes                         | No                              |

```bash
MY_DATA=${PWD}/mydata
echo ${MY_DATA}
mkdir -p ${MY_DATA}

docker run -it -v ${MY_DATA}:/data ubuntu bash
```

```bash
docker run -it -v my_volume1:/data ubuntu bash

docker volume ls
docker volume rm my_volume1
```


###  容器的網路環境

Docker daemon啟動之後，預設會建立一個新的router在電腦中，使用`172.17.0.0/16`的網段，分享網路資源給容器使用，與虛擬機預設的方式接近。

```bash
# 在主機上看網路狀態
ip addr

# 容器中看網路狀態
docker run -it --rm ubuntu bash
apt update && apt install -y iproute2
ip addr
```


### 建立容器時轉port

容器內部有開啟網頁服務時，可以使用`-p`參數設定port forward，把主機上的port對應到容器的port。如下方範例，`-p 80:80`表示把主機上的80 port映射到容器中的80 port。也可以從另一台電腦，網址打上你這台電腦的ip，連線到你的電腦裡的伺服器。

```bash
docker run -d -p 80:80 docker/getting-started
```

容器啟動後，打開瀏覽器開啟網頁 http://localhost 就會看到網頁內容。

主機port與容器port不須相同，例如使用`-p 8888:80`，開啟網頁時就要輸入 http://localhost:8888 才會看到網頁。


### 查看容器的log

容器在背景中執行，`docker ps`只會看到容器執行與否，不會看到容器程式的詳細輸出。不過Docker會集中所有容器的輸出內容，使用`docker logs`即可查看。

使用`-n 50`可以只列出最後50行輸出、使用`-f`參數可以持續輸出log(如果那個容器持續執行中的話)。

```bash
docker logs <容器名稱或id>

docker logs -n <數字> -f <容器名稱或id>
```


### 其他可能會用到的指令

#### Container執行相關 

```bash
# 查看help
docker --help
docker container --help

# run加上 --rm 的參數，可以在容器退出後自動刪除，不留下停止狀態的容器
docker run -it --rm <映像檔>

# run加上 -d 參數，可以在背景建立容器並執行。
docker run -d <映像檔>

# run加上 --name 可以設定容器名子，而不是隨機產生
docker run --name <容器名子> <映像檔>

# run加上 --restart 可以設定這容器掛掉的話重新啟動，也會在主機重新開機後自動執行。
docker run --restart <restart policy> <映像檔>
docker run --restart unless-stopped <映像檔>

# 檢視某個物件的詳細資料
docker inspect <容器 映像檔 或是volume>

# 把已經停止的容器再執行起來，會執行該容器的進入點。不過如果這個進入點又瞬間完成的話，容器還是會停止。
docker start <停止的容器>

# 把執行中的容器停止。
docker stop <執行中的容器>

# 在執行中的容器中執行一個新的指令，可以做別的事情。在這裡exit的話，原本進入點的程式仍繼續執行。
docker exec -it <執行中的容器> <新指令>

# 直接加入某個容器在執行中的程式，可以把執行中的程式Ctrl+C之類的。在這裡exit的話，容器就會停止。
# 如果要detach，要按Ctrl+P Ctrl+Q。
docker attach <執行中的容器>

# 在容器與主機間複製檔案
docker cp <來源> <目標>

docker cp hello.txt strange_brattain:/root      # 主機 -> 容器
docker cp strange_brattain:/root/hello.txt ./   # 容器 -> 主機
```


#### image相關

```bash
# 顯示image的相關用法
docker image --help

# 手動下載image
docker pull <映像檔名稱>

docker pull ubuntu          # 相當於ubuntu:latest
docker pull ubuntu:latest
docker pull ubuntu:focal    # 指定要focal這個tag的映像檔，而不是latest
docker pull ibmcom/ubuntu   # 由ibmcom使用者製作的映像檔，不是官方的
docker pull quay.io/centos/centos   # 下載來自quay.io網站的映像檔，而不是預設Docker Hub
docker pull quay.io/centos/centos:centos8   # 指定要centos8的tag的映像檔

# 為image重新命名（上新的tag）
docker tag <名稱或是id> <新名稱>

docker tag ubuntu:latest jeff/ubuntu:latest
docker tag feb5d9fea6a5 jeff/hello-world:latest

# 上傳image，push前要將映像檔改成正確名稱
docker push <映像檔名稱>

docker push jeff/hello-world:latest
docker push quay.io/jeff/hello-world:latest

# 把映像檔匯出成檔案
docker save -o <filename.tar> <image1> [<image2>...]    # 輸出到檔案，tar格式
docker save <image1> [<image2>...]      # 輸出到STDOUT，tar格式

docker save -o ubuntu.tar ubuntu:latest
docker save ubuntu:latest > ubuntu.tar

docker save ubuntu:latest | gzip > ubuntu.tar.gz    # 用pipe接上gzip壓縮，變成tgz檔
docker save ubuntu:latest | gzip | pv > ubuntu.tgz  # 再接上pv可以顯示壓縮狀態，適用於映像檔很大的情況
docker save ubuntu:latest | pigz | pv > ubuntu.tgz  # pigz是gzip壓縮的多執行緒版本，壓縮速度更快
# pigz和pv可能沒有內建，可以用apt安裝

# 把Docker映像檔的檔案匯入到Docker中
docker load -i <壓縮檔tar或tgz>    # 指定檔名讀取
docker load                       # 從STDIN讀取

docker load -i ubuntu.tgz
docker load < ubuntu.tgz

# 顯示映像檔製作的歷史
docker history <映像檔名或id>

# 看image的詳細資料
docker inspect <映像檔名或id>
```


### volume相關

```bash
# 顯示與volume相關的用法
docker volume --help

# 查看有哪些volume
docker volume ls

# 建立一個空的volume
docker volume create <新volume名子>

docker volume create volume1

# 刪除一個volume
docker volume rm <volume>

docker volume rm volume1

# 建立一個會掛載nfs的volume
docker volume create --driver local             \
    --opt type=nfs                              \
    --opt o=nfsvers=4,addr=192.168.123.45,rw    \
    --opt device=:/path/to/dir                  \
    volume-name

# 看volume的詳細資料
docker inspect <名稱或id>
docker volume inspect <volume名稱>

docker inspect volume1
docker volume inspect volume1
```
