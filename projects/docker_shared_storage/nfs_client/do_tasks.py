#!/usr/bin/env python3
import os
import argparse
import time
import fcntl
import pip
import importlib

try:
    yaml = importlib.import_module('yaml')
except ImportError:
    print('Installing yaml')
    pip.main(['install', 'pyyaml'])
    yaml = importlib.import_module('yaml')

from pathlib import Path, PurePath

def parse_args():
    tasks_dir_env = os.environ.get('TASKS_PATH')

    parser = argparse.ArgumentParser(description='Do the tasks.')
    parser.add_argument('-p', '--path', default=tasks_dir_env,
                        help='Task path.')
    args = parser.parse_args()
    return args

def do_task(task_dir):
    # 任務已完成就跳過
    if (task_dir / 'status_complete.txt').exists():
        return False

    ret = False

    try:
        # 嘗試開檔，開檔可能失敗
        with open(task_dir / 'status.txt', 'r+') as status_file:
            try:
                # 嘗試鎖住檔案
                fcntl.lockf(status_file, fcntl.LOCK_EX | fcntl.LOCK_NB)

                with open(task_dir / 'parameter.yml') as param_file:
                    sleep_data = yaml.safe_load(param_file)

                sleep_time = sleep_data['sleep_time']
                print(task_dir.name, 'start to sleep', sleep_time)
                time.sleep(sleep_time / 1000)

                # 工作完成後把檔案重新命名，別人會馬上無法開啟此檔
                # 接著舊檔過一陣子才會消失，新檔再過一陣子才會出現
                (task_dir / 'status.txt').rename(PurePath(task_dir) / 'status_complete.txt')

                print(task_dir.name, 'task done.')
                ret = True

            except OSError as e:
                print(task_dir.name, 'is locked', e)

            finally:
                fcntl.lockf(status_file, fcntl.LOCK_UN)
    except OSError as e:
        # FileNotFound Error ignored
        pass

    return ret

def do_all_tasks(args):
    task_dir_path = args.path
    p = Path(task_dir_path)
    if not p.is_dir():
        raise RuntimeError('Task path is not a directory')

    keep_going = False
    while True:
        keep_going = False

        task_list = sorted(p.glob('task*'))
        for task_dir in task_list:
            ret = False
            try:
                ret = do_task(task_dir)
            except Exception as e:
                print('Got exception', e)

            if ret:
                keep_going = True
                break

        if not keep_going:
            break
    pass

def main():
    args = parse_args()

    task_dir_path = args.path

    if not task_dir_path:
        raise RuntimeError('Task dir is not set')
    else:
        while True:
            do_all_tasks(args)
            print('Task done. Sleep for 30s')
            time.sleep(30)

if __name__ == '__main__':
    main()

