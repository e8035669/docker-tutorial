#!/bin/bash

for vmname in nfsserver client1 client2; do
    printf "%s" "ping ${vmname}"
    while ! ping -c1 -w1 ${vmname}.local &> /dev/null
    do
        printf "%s" "."
    done
    echo ""
done

echo "Remove known host keys..."
ssh-keygen -R nfsserver.local > /dev/null
ssh-keygen -R client1.local > /dev/null
ssh-keygen -R client2.local > /dev/null

echo "Get new host keys..."
ssh-keyscan nfsserver.local >> ${HOME}/.ssh/known_hosts 2> /dev/null
ssh-keyscan client1.local >> ${HOME}/.ssh/known_hosts 2> /dev/null
ssh-keyscan client2.local >> ${HOME}/.ssh/known_hosts 2> /dev/null

echo "Send files..."
scp -i id_ed25519 -r nfs_server/ ubuntu@nfsserver.local:
scp -i id_ed25519 -r nfs_client/ ubuntu@client1.local:
scp -i id_ed25519 -r nfs_client/ ubuntu@client2.local:

