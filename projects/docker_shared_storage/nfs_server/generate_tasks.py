#!/usr/bin/env python3
import os
from pathlib import Path
import argparse
import yaml
import glob
import random

def parse_arg():
    parser = argparse.ArgumentParser(description="Generate some sleep tasks.")
    parser.add_argument("-n", "--num", type=int, default=5, help="How many tasks to generate.")
    parser.add_argument("-p", "--path", default="data", help="Path to put tasks.")
    args = parser.parse_args()
    return args


def main():
    args = parse_arg()

    print("Prepare task directory.")
    os.makedirs(args.path, exist_ok=True)

    p = Path(args.path)
    exist_tasks = sorted(p.glob("task*"))
    print(exist_tasks)
    max_task_num = max([int(i.name[4:]) for i in exist_tasks if i.name[4:].isdigit()] + [0])

    for i in range(max_task_num + 1, max_task_num + 1 + args.num):
        print('Generate task{:04d}'.format(i))
        task_dir = p / 'task{:04d}'.format(i)
        task_dir.mkdir(exist_ok=True)

        param_data = {
            'sleep_time': random.randint(5, 20) * 1000
        }
        param_file = task_dir / 'parameter.yml'
        with open(param_file, 'w') as f:
            yaml.safe_dump(param_data, f, indent=4)

        (task_dir / 'status.txt').touch()

if __name__ == "__main__":
    main()
