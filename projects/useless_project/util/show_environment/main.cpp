#include <algorithm>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <map>
#include <sstream>
#include <string>

using namespace std;
namespace fs = std::filesystem;

bool check_docker() {
    bool ret = fs::exists(fs::path("/.dockerenv"));
    if (!ret) {
        std::ifstream ifs("/proc/self/cgroup");
        for (std::string s; std::getline(ifs, s);) {
            if (s.find("docker") != std::string::npos) {
                ret = true;
                break;
            }
        }
    }

    return ret;
}

std::map<std::string, std::string> read_env_file(const std::string& path) {
    std::map<std::string, std::string> ret;
    std::ifstream ifs(path);
    for (std::string s; std::getline(ifs, s);) {
        if (s.length() == 0 || s.at(0) == '#') {
            continue;
        }
        std::string::size_type pos = s.find("=");
        if (pos == std::string::npos) {
            continue;
        }
        std::string key = s.substr(0, pos);
        std::string value = s.substr(pos + 1);
        if (value[0] == '"' && value[value.length() - 1] == '"') {
            value = value.substr(1, value.size() - 2);
        }
        ret[key] = value;
    }

    return ret;
}

int main(int argc, char** argv) {
    bool is_docker = check_docker();
    if (is_docker) {
        cout << "You are in docker container." << endl;
    } else {
        cout << "You are not in docker container." << endl;
    }

    if (fs::exists("/etc/hostname")) {
        ifstream ifs("/etc/hostname");
        string hostname;
        ifs >> hostname;
        cout << "Your hostname is " << hostname << endl;
    }

    string info_file = "/etc/os-release";
    if (fs::exists(info_file)) {
        auto infos = read_env_file(info_file);
        auto it = infos.find("PRETTY_NAME");
        if (it == infos.end()) {
            it = infos.find("NAME");
        }
        if (it != infos.end()) {
            cout << "You are running " << it->second << endl;
        } else {
            cout << "You are running unknown OS." << endl;
        }
    } else {
        cout << "Not a valid OS." << endl;
    }

    return 0;
}
