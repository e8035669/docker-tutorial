# Docker Tutorial

這篇Docker Tutorial製作給高雄大學ICAL實驗室的夥伴。教學內容比較適合研究生在研究環境的製作、開發者在軟體的自動化測試的情境。與官方的教學做給雲端服務的人的使用情境不相同。因此教學裡面在網路方面的著墨較少、在映像檔製作方面著墨較多。希望夥伴寫的程式到哪都可以順利編譯+執行嘿。

## 目錄

1. [Docker介紹+安裝教學](01-docker-introduce.md)
2. [Docker基本指令介紹(Docker cli)](02-docker-cli.md)
3. [Dockerfile](03-docker-dockerfile.md)
4. [Docker + GPU](04-docker-gpu.md)
5. [Docker Compose](05-docker-compose.md)
6. [Docker + Shared Storage](06-docker-nfs.md)
