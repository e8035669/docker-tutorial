# Docker + GPU

大家有時會有訓練模型需要顯卡的需求，但一定是僧多粥少的情況。顯卡是大家共用，要把訓練程式搬到有顯卡上的電腦執行。每個人都想在訓練主機上建立自己的環境，結果就是在主機上搞的套件版本大亂，最後大家都很難用。

使用anaconda或是miniconda其實是一個不錯的解法，不過這主要還是python使用者在用的，套件要依賴anaconda的repo才行。如果你的訓練程式是用C語言，套件管理搭配anaconda可能有點奇怪。如果使用Docker的話，依賴的套件管理可以是apt，使用上相對來的方便。


## 安裝NVIDIA Container Toolkit

給Ubuntu使用者

安裝教學在nvidia官網有：https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html#docker

~~呃…指令很長看不懂，複製貼上就對了~~

```bash
distribution=$(. /etc/os-release;echo $ID$VERSION_ID) \
      && curl -fsSL https://nvidia.github.io/libnvidia-container/gpgkey | sudo gpg --dearmor -o /usr/share/keyrings/nvidia-container-toolkit-keyring.gpg \
      && curl -s -L https://nvidia.github.io/libnvidia-container/$distribution/libnvidia-container.list | \
            sed 's#deb https://#deb [signed-by=/usr/share/keyrings/nvidia-container-toolkit-keyring.gpg] https://#g' | \
            sudo tee /etc/apt/sources.list.d/nvidia-container-toolkit.list

# 安裝套件
sudo apt-get update
sudo apt-get install -y nvidia-docker2

# 重新啟動Docker daemon
sudo systemctl restart docker
```

Archlinux或Manjaro的使用者，有個`nvidia-container-toolkit`套件在AUR上，要使用支援AUR的套件管理程式來安裝。

```bash
yay -S nvidia-container-toolkit
# 或是
pacaur -S nvidia-container-toolkit
```

測試nvidia docker

```bash
docker run --rm --gpus all nvidia/cuda:11.0-base nvidia-smi
```

如果看的到`nvidia-smi`的訊息，那表示容器中已可使用顯卡。

```bash
docker run -it --rm --gpus all nvidia/cuda:11.0-base bash

# 進到容器中
mount | grep nvidia
```

![gpu_img03.png](assets/gpu_img03.png)

可以看到mount中有許多與driver相關的檔案從主機上掛載到容器中，讓顯卡可以在容器裡面被使用


## 在容器中使用NVIDIA顯卡

這邊來試試看在容器中簡單的訓練一個模型，因為作者比較習慣pytorch所以就以pytorch為例。

如果是用python寫訓練模型的程式的話，通常不用自己準備環境，因為TensorFlow, PyTorch, MXNet, PaddlePaddle這些比較有名的框架，官方都替你準備好映像檔了，樓下僅列出我知道的。

- TensorFlow: https://hub.docker.com/r/tensorflow/tensorflow
- PyTorch: https://hub.docker.com/r/pytorch/pytorch
- PyTorch Lightning: https://hub.docker.com/r/pytorchlightning/pytorch_lightning
- MXNet: https://hub.docker.com/r/mxnet/python
- PaddlePaddle: https://hub.docker.com/r/paddlepaddle/paddle
- TensorFlow: https://catalog.ngc.nvidia.com/orgs/nvidia/containers/tensorflow
- PyTorch: https://catalog.ngc.nvidia.com/orgs/nvidia/containers/pytorch
- MXNet: https://catalog.ngc.nvidia.com/orgs/nvidia/containers/mxnet

主機上只要有安裝nvidia驅動程式(nvidia-driver)、nvidia-docker2和docker，這些映像檔就能夠正常執行，主機中不需要安裝CUDA或CUDNN，因為他們是函式庫，必須被包在映像檔裡。

要有心裡準備的是，這些映像檔解壓縮後動輒5G起跳，也可能到10G以上，因為CUDA+CUDNN已經佔用2~3G容量，再加上這些框架的程式碼就使得映像檔更加龐大。


```bash
docker pull pytorch/pytorch:1.11.0-cuda11.3-cudnn8-runtime

cd projects/basic_training
docker run -it --gpus all -v ${PWD}:/workspace pytorch/pytorch:1.11.0-cuda11.3-cudnn8-runtime bash

# 進到容器中之後
python train_cnn.py
```

這個範例會下載Fashion MNIST，並且訓練一個簡單的CNN模型，儲存在當前資料夾底下`best_model.pt`，這個模型不太好，只有75%左右的正確率。

![gpu_img01.png](assets/gpu_img01.png)
![gpu_img02.png](assets/gpu_img02.png)


## 使用上的注意事項

1. 如果使用NN的框架，有可能會遇到`/dev/shm`空間不足的問題，這是memory disk的區域，docker預設分配64MB的容量。如果遇到不夠用，容器要刪掉重建，加入` --ipc=host`或是`--shm-size=1G`的參數去調整memory disk的容量大小。
2. 自己製作映像檔的話，映像檔裡不要安裝nvidia driver，因為這些檔案會自動從主機上掛載，有安裝的話會有版本問題讓容器內抓不到顯卡。


## 製作客製化的映像檔-1

這邊想要製作一個內建pytorch + yolov5的映像檔，而且pytorch要有支援GPU。我們以`nvidia/cuda`當作parent image開始製作。

製作映像檔要注意幾個狀況
1. 不可以有詢問yes or no的狀況出現。因為製作映像檔的時候不能與容器互動，出現yes or no要按的時候也會沒辦法按。
2. 映像檔為了讓體積小，很多電腦裡預裝的函式庫可能都會沒有，而如果套件會用到就會找不到函式庫，例如`libgl.so`，要額外安裝。

這個映像檔在製作的過程中碰到tzdata會跳出時區選擇的視窗，於是把tzdata的安裝特別獨立出來。後面又遇到`yolov5`有使用`cv2`，而`cv2`需要`libgl.so`。這個library在ubuntu安裝都會內建，但ubuntu的docker映像檔並不會內建，所以上網搜尋之後把要需要的套件補回去。

搞定這類型的小問題之後，製作映像檔都很簡單，下方的Dockerfile能夠製作出完整的映像檔，在容器中可以跑yolov5。

```Dockerfile
FROM nvidia/cuda:11.3.0-runtime-ubuntu20.04
# FROM nvidia/cuda:11.3.0-cudnn8-runtime-ubuntu20.04

RUN sed -i 's/http:\/\/archive.ubuntu.com\/ubuntu/http:\/\/free.nchc.org.tw\/ubuntu/g' /etc/apt/sources.list

# tzdata need to be install first
# https://stackoverflow.com/questions/44331836/apt-get-install-tzdata-noninteractive
RUN ln -fs /usr/share/zoneinfo/Asia/Taipei /etc/localtime       \
 && apt-get update                                              \
 && DEBIAN_FRONTEND=noninteractive apt-get install -y tzdata    \
 && dpkg-reconfigure --frontend noninteractive tzdata           \
 && rm -rf /var/lib/apt/lists/*

# OpenCV needs ffmpeg libsm6 libxext6
# https://stackoverflow.com/questions/55313610/importerror-libgl-so-1-cannot-open-shared-object-file-no-such-file-or-directo
RUN apt-get update                                                  \
 && apt-get install -y                                              \
        python3-pip git python-is-python3                           \
        ffmpeg libsm6 libxext6                                      \
 && rm -rf /var/lib/apt/lists/*

RUN python3 -m pip --no-cache-dir install torch torchvision         \
        --extra-index-url https://download.pytorch.org/whl/cu113

RUN mkdir -p /tools/ && cd /tools                               \
 && git clone --depth=1 https://github.com/ultralytics/yolov5   \
 && cd yolov5                                                   \
 && python3 -m pip --no-cache-dir install -r requirements.txt

WORKDIR /workspace
```

![](assets/gpu_img04.png)


## 製作客製化的映像檔-2

這邊想要製作一個內建AlexeyAB的Darknet，並且Darknet要先編譯好。這次開頭選擇了`devel`版本映像檔，表示開發(develop)用途，映像檔內包含nvcc, gcc等編譯工具，可用來編譯Darknet。

Darknet編譯好之後，其實nvcc或gcc還有大量的header檔都不需要了，這邊使用了multi-stage builds的技巧。第二階段從`runtime`版本的映像檔開始，再把第一階段編譯好的darknet複製過來放好。這個技巧能夠減少映像檔的層數、容量。


```Dockerfile
FROM nvidia/cuda:11.3.0-devel-ubuntu20.04 AS builder
# FROM nvidia/cuda:11.3.0-cudnn8-devel-ubuntu20.04

RUN apt-get update && apt-get install -y git    \
 && rm -rf /var/lib/apt/lists/*

RUN mkdir -p /tools && cd /tools                                        \
 && git clone https://github.com/AlexeyAB/darknet.git --depth=1         \
 && cd darknet                                                          \
 && sed -i s/GPU=0/GPU=1/g Makefile                                     \
 && sed -i s/AVX=0/AVX=1/g Makefile                                     \
 && sed -i s/OPENMP=0/OPENMP=1/g Makefile                               \
 && sed -i '25i ARCH= -gencode arch=compute_61,code=[sm_61,compute_61]  \
         -gencode arch=compute_75,code=[sm_75,compute_75]               \
         -gencode arch=compute_86,code=[sm_86,compute_86]' Makefile     \
 && make -j4 && rm -rf obj

FROM nvidia/cuda:11.3.0-runtime-ubuntu20.04

RUN apt-get update && apt-get install -y libgomp1    \
 && rm -rf /var/lib/apt/lists/*

COPY --from=builder /tools /tools

WORKDIR /workspace
```


## 其他參考資料

 - Use multi-stage builds: https://docs.docker.com/develop/develop-images/multistage-build/
 - Dockerfile best practies: https://docs.docker.com/develop/develop-images/dockerfile_best-practices/
 - nvidia/cuda Docker Hub: https://hub.docker.com/r/nvidia/cuda
