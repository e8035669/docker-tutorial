# Dockerfile

Dockerfile是用來製作映像檔的腳本，為了製作客製化的映像檔給自己，Dockerfile是一定要會的。製作好的映像檔可以上傳至Docker Hub，可以方便的在別台電腦下載使用，在那台電腦中也能有相同環境。

## 製作一個啥都沒有的映像檔

首先，建立一個空的資料夾，裡面建立一個叫做`Dockerfile`的檔案。

```bash
mkdir simple_image && cd simple_image
vim Dockerfile
```

在`Dockerfile`檔案中寫入這一行，寫完之後存檔。每一個映像檔在製作之前，必定從一個`FROM`開始，指定要以哪一個映像檔為基底。

```Dockerfile
FROM ubuntu:22.04
```

接下來使用`docker build`指令來製作映像檔。

```bash
docker build . -t simple_image
```

![dockerfile_img01.png](assets/dockerfile_img01.png)

試試以新的映像檔建立容器。

```bash
docker run -it simple_image
```

## 製作一個有內建gcc的映像檔

把剛剛的`Dockerfile`加點東西，在映像檔製作過程中安裝C/C++的編譯器以及常用工具。

```Dockerfile
FROM ubuntu:22.04

RUN apt-get update
RUN apt-get install -y build-essential cmake git
```

再製作一次映像檔。

```bash
docker build . -t simple_image
```

接著建立容器，把專案掛載到容器中，試著編譯和執行。

```bash
cd projects/useless_project
docker run -it -v ${PWD}:/workspace simple_image

# 在容器中
cd /workspace
mkdir -p build && cd build
cmake .. && make install -j4
./install/bin/show_environment
```

Docker映像檔的儲存是以一層一層的方式儲存，Dockerfile裡的每一個`RUN`, `ADD`或是`COPY`的指令都會為映像檔增加一個新的Layer，儲存目錄中有改變的地方，並且佔用容量。

前面的`Dockerfile`有個不良的地方，通常套件管理程式在執行的過程中會產生快取檔案，Ubuntu的`apt`, CentOS的`dnf`或是python使用的`pip`都會在安裝過程中留下套件庫清單以及安裝過的套件包。製作映像檔時的好習慣是確保映像檔裡只有必要檔案，不必要的檔案要清除，並且指令盡量精簡。我們來修改一下前面的`Dockerfile`。

```Dockerfile
FROM ubuntu:22.04

RUN sed -i 's/http:\/\/archive.ubuntu.com\/ubuntu/http:\/\/free.nchc.org.tw\/ubuntu/g' /etc/apt/sources.list

RUN apt-get update && apt-get install -y      \
    build-essential cmake git                 \
 && rm -rf /var/lib/apt/lists/*

WORKDIR /workspace
```

製作映像檔的建議：

1. 絕對不要把個人資料放在映像檔中，尤其是帳號密碼。
2. 映像檔製作過程中不能有詢問yes or no出現，`apt`安裝一定要加`-y`。
3. 映像檔製作時要清除暫存檔案，映像檔才不會很大。
4. 映像檔製作時Layer能少盡量少。

還有官方的建議：https://docs.docker.com/develop/dev-best-practices/

Dockerfile還有很多其他指令，這邊不一一細講。常用的指令還有:
 - `ADD`，可以將專案底下的檔案直接加入映像檔中。
 - `ENTRYPOINT`，可以修改映像檔的預設進入點。
 - `ENV`，可以幫映像檔新增環境變數，並且保留在映像檔中。
 - `ARG`，可以接收來自`docker build`指令中指定的變數，但這個變數不會保存在最後的映像檔裡。
 - `EXPOSE`，宣告映像檔可能用來提供服務的network port，不宣告也沒關係，實際要不要轉port還是要用`docker run`的`-p`參數才能轉port。

若有需要用到，可以參考：https://docs.docker.com/engine/reference/builder/


## 把映像檔上傳Docker Hub

Docker Hub是一個Docker官方的映像檔儲存倉庫，上面有大量的映像檔可以使用，每位使用者都能自己建立帳號並上傳映像檔。不過Docker Hub的免費帳號是有限制的，例如只能使用一個Private Repo、每個IP有流量限制。後面的範例會上傳映像檔到Docker Hub，所以可以先到Docker Hub的官方網站[hub.docker.com](https://hub.docker.com/)，建立一個新帳號。

儲存倉庫不是只有Docker Hub，這邊舉例其他的映像檔儲存倉庫：

 - [Harbor](https://goharbor.io/)，可以自己架設的映像檔倉庫。
 - Gitlab，映像檔倉庫整合在每個專案底下，[參考連結](https://docs.gitlab.com/ee/user/packages/container_registry/)。
 - quay.io，Red Hat做的映像檔倉庫，似乎是商業用的，要$$。

要將映像檔上傳有幾件事情要準備：

1. 用`docker login`登入網站，登入docker hub或是其他儲存倉庫，每台電腦要做一次。
2. 用`docker tag`把映像檔名稱改成正確格式。
3. 用`docker push`把映像檔上傳。

```bash
# 要上傳Docker官方的話的格式
<使用者名稱>/<映像檔名稱>:<Tag>

# 要上傳其他網站的話，最一開頭一定是附上網址，後面的使用者名稱格式就不一定了。
# 映像檔名稱裡面可以包含更多斜線(/)。
<網站網址>/<使用者名稱>/<映像檔名稱>:<Tag>
```

```bash
docker tag simple_image <你的使用者名稱>/simple_image:v1
docker login

docker push <剛剛的映像檔名稱>
```

![dockerfile_img02.png](assets/dockerfile_img02.png)

上傳後的映像檔就會**公開**在docker hub上面，可以在網站上看到自己剛才上傳的映像檔。

![dockerfile_img03.png](assets/dockerfile_img03.png)

![dockerfile_img04](assets/dockerfile_img04.png)

順帶一題，下載映像檔的指令是用`docker pull`，如果映像檔是非公開的，有可能需要先用`docker login`登入。可以在別台電腦下這個指令，就會把映像檔下載在另一台電腦裡。

```bash
docker pull <映像檔名稱>
```


## 映像檔的匯出與匯入

映像檔不一定要上傳到Docker Hub，雖然他的資源豐富，上傳下載也很方便，可他還是有流量限制。映像檔也能夠匯出成壓縮檔，用這個檔案在別台電腦匯入。這邊會使用`docker save`還有`docker load`去匯出和匯入映像檔。

```bash
docker save -o <檔名.tar> <映像檔名稱>...

docker save -o simple_image.tar simple_image:latest
```

匯出過程不會有任何提示，完成之後就會在目錄底下看到`simple_image.tar`，這個檔案裡面就是匯出的映像檔。用`docker load`就能匯入映像檔。

```bash
docker load -i <檔名>

docker load -i simple_image.tar
```

匯出的格式是tar檔，其實這個格式沒有壓縮功能，匯出的檔案大小一定比較大。若要壓縮的話，可以善用pipe的功能，搭配linux底下習慣用gzip來壓縮，壓縮完就會變成常見的`.tgz`檔或是`.tar.gz`檔。至於匯入檔案的部份，`docker load`可以直接接受`tgz`格式。

```bash
docker save <映像檔名稱> | gzip > <檔名.tgz>

docker save simple_image:latest | gzip > simple_image.tgz
docker load -i simple_image.tgz
```

但是如果你一次匯出很多個映像檔，又或是你的映像檔超大，然後匯出檔案又一點訊息也沒有，你可能會等到懷疑人生。這邊建議可以搭配`pv`這個指令，可以顯示動畫，~~舒緩焦慮現象~~。再用`pigz`來取代`gzip`，因為`pigz`支援多執行緒的壓縮，速度更快。`pv`和`pigz`要自己用apt安裝，Ubuntu可能不會內建。

```bash
docker save simple_image:latest | pigz | pv > simple_image.tgz
```


## 有關於映像檔的儲存方式

Docker映像檔的儲存是以層層堆疊的方式儲存，Dockerfile裡的每一個`RUN`, `ADD`或是`COPY`的指令都會為映像檔**疊**上一個新的Layer，儲存目錄中有改變的地方，並且佔用容量。如果你在下層建立一個大檔案，在上層將檔案刪除，實際上只會在上層有刪除的一筆紀錄，但並不會真的去把下層的檔案刪掉，當然也不會釋放空間。所以在寫Dockerfile的時候，步驟要盡量精鍊、簡潔，而且安裝完套件之後留下來的安裝包盡量都要刪掉。每一個`RUN`裡面都會有類似下載、安裝、**清理**三個動作。

當你的映像檔製作完成之後，你用映像檔建立了容器。Docker會在映像檔之上再開一個可讀寫的Layer，在容器裡的任何操作，包含寫檔、刪除檔案，都會紀錄在這個Layer上。底下的image其實不會有任何改變

![container-layers.jpg](https://docs.docker.com/storage/storagedriver/images/container-layers.jpg)

當你用一個映像檔建立了多個容器，每個容器都會有自己一個讀寫的Layer互不干擾。當你刪除了容器，也會一併把最上層的讀寫Layer刪除，底下的image仍然不變。

![sharing-layers.jpg](https://docs.docker.com/storage/storagedriver/images/sharing-layers.jpg)


## 之前在實驗室製作的，內建有編譯環境和實驗室常用的library的映像檔

可用來跟Gitlab CI/CD功能整合的，在使用者push新的程式碼之後，Gitlab runner會自動拿這個映像檔來編譯你的code，也可執行一些測試程式，確保你寫的程式碼是可以在別台電腦上正確編譯、正確執行的。

 - https://hub.docker.com/r/ical/source
 - https://gitlab.ical.tw/e8035669/ical-source (這專案寫的不好，密碼在程式碼裡，不要外流。)


## 其他參考資料

 - Dockerfile reference: https://docs.docker.com/engine/reference/builder/
 - Docker development best practices: https://docs.docker.com/develop/dev-best-practices/
 - About storage drivers: https://docs.docker.com/storage/storagedriver/
 - GitLab Container Registry: https://docs.gitlab.com/ee/user/packages/container_registry/
 - Harbor: https://goharbor.io/
